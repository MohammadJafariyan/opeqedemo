import React from 'react';
import App from "../App";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch,
    useParams
} from "react-router-dom";

import {Index} from "./Index";
import {BasePage} from "./BasePage";
import {MyHead} from "./MyHead";
import {Reservation} from "./Reservation";
import {Locations} from "./Locations";
import {Login} from "./Login";
import {Orders} from "./Orders";
import {SignUp} from "./SignUp";


export class Layout extends  BasePage{


    render() {
        return (
            <div>

                <MyHead/>
                <Router>
                    <div>
                        <ul>
                            <li><Link to="/">Home</Link></li>
                            <li><Link to="/about">About</Link></li>
                            <li><Link to="/contact">Contact</Link></li>
                        </ul>
                        <hr/>

                        <Route exact path="/" component={Index}/>
                        <Route exact path="/Reservation" component={Reservation}/>
                        <Route exact path="/Locations" component={Locations}/>
                        <Route exact path="/Login" component={Login}/>
                        <Route exact path="/Orders" component={Orders}/>
                        <Route exact path="/SignUp" component={SignUp}/>
                        {/* // All 3 components below would be rendered when in a homepage
                        <Route exact path="/" component={Home}/>
                        <Route exact path="/" component={About}/>
                        <Route exact path="/" component={Contact}/>

                        <Route path="/about" component={About}/>
                        <Route path="/contact" component={Contact}/>*/}
                    </div>
                </Router>
            </div>
        );
    }
}