import React from 'react';

import {BasePage} from "./BasePage";


export class MyHead extends BasePage {
    render() {
        return (
            <nav className="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
                <div className="container">
                    <button data-toggle="collapse" className="navbar-toggler" data-target="#navcol-1"><span
                        className="sr-only">Toggle navigation</span><span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navcol-1"><img
                        src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo.png" />
                        <ul className="nav navbar-nav ml-auto">
                            <li className="nav-item" role="presentation"><a className="nav-link active"
                                                                            href="/"><br/>Home<br/><br/></a>
                            </li>

                            <li className="nav-item" role="presentation"><a className="nav-link active"
                                                                            href="Reservation"><br/>Reservation<br/><br/></a>
                            </li>
                            <li className="nav-item" role="presentation"><a className="nav-link"
                                                                            href="Orders"><br/>Orders<br/><br/></a>
                            </li>
                            <li className="nav-item" role="presentation"><a className="nav-link"
                                                                            href="Locations"><br/>Locations<br/><br/></a>
                            </li>
                            <li className="nav-item" role="presentation"><a className="nav-link"
                                                                            href="login"><br/>log
                                in

                                <br/><br/></a>
                            </li>
                            <li className="nav-item" role="presentation"><a className="nav-link" href="signup"><br/>Sign
                                up</a></li>
                            <li className="nav-item" role="presentation"><a className="nav-link" href="contact-us.html"><br/><i
                                className="fa fa-shopping-basket" style={{width: '18px',fontSize: '20px'}}></i>
                                <br/></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

    );
    }

    }