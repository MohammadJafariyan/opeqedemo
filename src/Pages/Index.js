import React from 'react';

import {BasePage} from "./BasePage";
import {Carousel} from "../Components/Carousel";
import {Footer} from "../Components/Footer";


export class Index extends  BasePage {

    render() {
        return (
            <div>
               <Carousel />

               <Footer/>
            </div>
        );
    }

}