import React from 'react';
import {BasePage} from "../Pages/BasePage";
import {Part1} from "./Part1";
import {Part2} from "./Part2";
import {Part3} from "./Part3";



export class Carousel extends BasePage {

    render() {
        return (
            <main className="page landing-page"><img
                src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo-9.png" />
             <Part1/>
             <Part2/>
             <Part3/>
            </main>

    );
    }
}