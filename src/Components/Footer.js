import {BasePage} from "../Pages/BasePage";
import React from "react";

export class Footer extends BasePage {
    render() {
        return (
            <>
                <p className="text-center"><br/>br/owse our menu for dine-in, delivery or pickup and catering<br/></p>
                <p className="text-center"><br/><br/><a href="https://demo.opeqe.com/order-options">ASAP PickupBeverly
                    Hills - 1008 Elden Way</a>&nbsp; &nbsp; &nbsp;
                    <button className="btn btn-primary" type="button" style={{backgroundColor: 'rgb(27,28,30)'}}>change
                    </button>
                    &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp;<a href="#">Delivery</a>&nbsp; &nbsp; &nbsp; or <a
                        href="#">pickup</a>&nbsp;<br/><br/></p>
                <img src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo-7.png" width="100%"/>
                <footer className="page-footer dark">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-3">
                                <h5>Get started</h5>
                                <ul>
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Sign up</a></li>
                                    <li><a href="#">Downloads</a></li>
                                </ul>
                            </div>
                            <div className="col-sm-3">
                                <h5>About us</h5>
                                <ul>
                                    <li><a href="#">Company Information</a></li>
                                    <li><a href="#">Contact us</a></li>
                                    <li><a href="#">Reviews</a></li>
                                </ul>
                            </div>
                            <div className="col-sm-3">
                                <h5>Support</h5>
                                <ul>
                                    <li><a href="#">FAQ</a></li>
                                    <li><a href="#">Help desk</a></li>
                                    <li><a href="#">Forums</a></li>
                                </ul>
                            </div>
                            <div className="col-sm-3">
                                <h5>Legal</h5>
                                <ul>
                                    <li><a href="#">Terms of Service</a></li>
                                    <li><a href="#">Terms of Use</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="footer-copyright">
                        <p>© 2018 Copyright Text</p>
                    </div>
                </footer>
            </>
        );
    }
}