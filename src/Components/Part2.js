import {BasePage} from "../Pages/BasePage";
import React from "react";

export class Part2 extends BasePage {


    render() {
        return (
            <>
                <section className="clean-block features">
                    <div className="container">
                        <div className="block-heading"><img
                            src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo-4.png"
                            width="100%" />
                            <p></p>
                        </div>
                    </div>
                </section>

            </>
        );
    }
}