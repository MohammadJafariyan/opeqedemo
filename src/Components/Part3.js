import {BasePage} from "../Pages/BasePage";
import React from "react";

export class Part3 extends BasePage {


    render() {
        return (
            <>
                <div className="container">
                    <div className="block-heading">
                        <h2 className="text-left text-info"><br/>Special Offers<br/></h2>
                        <p></p>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-sm-6 col-lg-4">
                            <div className="card clean-card text-center"><img className="card-img-top w-100 d-block"
                                                                              src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo-1.png"/>
                                <div className="card-body info">
                                    <h4 className="card-title"><br/>T-Bone Steak &amp; Eggs<br/><br/></h4>
                                    <p className="card-text"><a
                                        href="https://demo.opeqe.com/location/beverly-hills/search/menu/pizza">Pizza</a><a
                                        href="https://demo.opeqe.com/location/beverly-hills/search/cuisine/italian">Italian</a><a
                                        href="https://demo.opeqe.com/location/beverly-hills/search/course/main-course">Main
                                        Course</a><br/></p>
                                    <div
                                        className="icons"><a href="#"><i className="icon-social-facebook"></i></a><a
                                        href="#"><i className="icon-social-instagram"></i></a><a href="#"><i
                                        className="icon-social-twitter"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-6 col-lg-4">
                            <div className="card clean-card text-center"><img className="card-img-top w-100 d-block"
                                                                              src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo-2.png"/>
                                <div className="card-body info">
                                    <h4 className="card-title">Robert Downturn</h4>
                                    <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing
                                        elit.</p>
                                    <div className="icons"><a href="#"><i className="icon-social-facebook"></i></a><a
                                        href="#"><i className="icon-social-instagram"></i></a><a href="#"><i
                                        className="icon-social-twitter"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-6 col-lg-4">
                            <div className="card clean-card text-center"><img className="card-img-top w-100 d-block"
                                                                              src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo-3.png"/>
                                <div className="card-body info">
                                    <h4 className="card-title">Ally Sanders</h4>
                                    <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing
                                        elit.</p>
                                    <div className="icons"><a href="#"><i className="icon-social-facebook"></i></a><a
                                        href="#"><i className="icon-social-instagram"></i></a><a href="#"><i
                                        className="icon-social-twitter"></i></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="block-heading">
                        <p></p>
                    </div>
                    <div className="row justify-content-around">
                        <div
                            className="col-6 col-sm-6 col-lg-4 d-flex justify-content-center align-items-center align-content-center flex-wrap">
                            <img className="justify-content-center align-items-center align-content-center"
                                 src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo-5.png"/>
                        </div>
                        <div className="col-6 col-sm-6 col-lg-4">
                            <div className="card clean-card text-center">
                                <div className="card-body info">
                                    <h4 className="card-title"><br/>br/anded&nbsp;Gift Card<br/><img
                                        src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo-6.png" /><br/>
                                    </h4>
                                    <p className="card-text"><br/>Opeqe provides a wide range of customizable products
                                        for reward and incentive programs that can meet your restaurant’s goals.<br/>Whether<br/> you
                                        are looking to drive the addition of new customers, increase the <br/>loyalty
                                        of existing ones we have a customized solution for you.<br/><br/></p>
                                    <div className="icons"><a href="#"><i className="icon-social-facebook"></i></a><a
                                        href="#"><i className="icon-social-instagram"></i></a><a href="#"><i
                                        className="icon-social-twitter"></i></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div className="row">
                        <div className="col">
                            <h5 className="text-center"><br/>Ready to order?<br/><br/></h5>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}


