import React from 'react';
import {BasePage} from "../Pages/BasePage";


export class Part1 extends BasePage {


    render() {
        return (
            <>
                <section className="clean-block clean-info dark">
                    <section className="clean-block about-us">
                        <div className="container">
                            <div className="block-heading">
                                <h2 className="text-left text-info"><br/>Special Offers<br/></h2>
                                <p></p>
                            </div>
                            <div className="row justify-content-center">
                                <div className="col-sm-6 col-lg-4">
                                    <div className="card clean-card text-center"><img
                                        className="card-img-top w-100 d-block"
                                        src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo-1.png"/>
                                        <div className="card-body info">
                                            <h4 className="card-title"><br/>T-Bone Steak &amp; Eggs<br/><br/></h4>
                                            <p className="card-text"><a
                                                href="https://demo.opeqe.com/location/beverly-hills/search/menu/pizza">Pizza</a><a
                                                href="https://demo.opeqe.com/location/beverly-hills/search/cuisine/italian">Italian</a><a
                                                href="https://demo.opeqe.com/location/beverly-hills/search/course/main-course">Main
                                                Course</a><br/></p>
                                            <div
                                                className="icons"><a href="#"><i
                                                className="icon-social-facebook"></i></a><a href="#"><i
                                                className="icon-social-instagram"></i></a><a href="#"><i
                                                className="icon-social-twitter"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-lg-4">
                                    <div className="card clean-card text-center"><img
                                        className="card-img-top w-100 d-block"
                                        src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo-2.png"/>
                                        <div className="card-body info">
                                            <h4 className="card-title">Robert Downturn</h4>
                                            <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing
                                                elit.</p>
                                            <div className="icons"><a href="#"><i className="icon-social-facebook"></i></a><a
                                                href="#"><i className="icon-social-instagram"></i></a><a href="#"><i
                                                className="icon-social-twitter"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-lg-4">
                                    <div className="card clean-card text-center"><img
                                        className="card-img-top w-100 d-block"
                                        src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo-3.png"/>
                                        <div className="card-body info">
                                            <h4 className="card-title">Ally Sanders</h4>
                                            <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing
                                                elit.</p>
                                            <div className="icons"><a href="#"><i className="icon-social-facebook"></i></a><a
                                                href="#"><i className="icon-social-instagram"></i></a><a href="#"><i
                                                className="icon-social-twitter"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="block-heading">
                                <h2 className="text-left text-info"><br/>Special Offers<br/></h2>
                                <p></p>
                            </div>
                            <div className="row justify-content-center">
                                <div className="col-sm-6 col-lg-4">
                                    <div className="card clean-card text-center"><img
                                        className="card-img-top w-100 d-block"
                                        src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo-1.png"/>
                                        <div className="card-body info">
                                            <h4 className="card-title"><br/>T-Bone Steak &amp; Eggs<br/><br/></h4>
                                            <p className="card-text"><a
                                                href="https://demo.opeqe.com/location/beverly-hills/search/menu/pizza">Pizza</a><a
                                                href="https://demo.opeqe.com/location/beverly-hills/search/cuisine/italian">Italian</a><a
                                                href="https://demo.opeqe.com/location/beverly-hills/search/course/main-course">Main
                                                Course</a><br/></p>
                                            <div
                                                className="icons"><a href="#"><i
                                                className="icon-social-facebook"></i></a><a href="#"><i
                                                className="icon-social-instagram"></i></a><a href="#"><i
                                                className="icon-social-twitter"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-lg-4">
                                    <div className="card clean-card text-center"><img
                                        className="card-img-top w-100 d-block"
                                        src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo-2.png"/>
                                        <div className="card-body info">
                                            <h4 className="card-title">Robert Downturn</h4>
                                            <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing
                                                elit.</p>
                                            <div className="icons"><a href="#"><i className="icon-social-facebook"></i></a><a
                                                href="#"><i className="icon-social-instagram"></i></a><a href="#"><i
                                                className="icon-social-twitter"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-lg-4">
                                    <div className="card clean-card text-center"><img
                                        className="card-img-top w-100 d-block"
                                        src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo-3.png"/>
                                        <div className="card-body info">
                                            <h4 className="card-title">Ally Sanders</h4>
                                            <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing
                                                elit.</p>
                                            <div className="icons"><a href="#"><i className="icon-social-facebook"></i></a><a
                                                href="#"><i className="icon-social-instagram"></i></a><a href="#"><i
                                                className="icon-social-twitter"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </section>
                <section className="clean-block features">
                    <div className="container">
                        <div className="block-heading">
                            <h2 className="text-left text-info"><br/>Special Offers<br/></h2>
                            <p></p>
                        </div>
                        <div className="row justify-content-center">
                            <div className="col-sm-6 col-lg-4">
                                <div className="card clean-card text-center"><img className="card-img-top w-100 d-block"
                                                                                  src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo-1.png"/>
                                    <div className="card-body info">
                                        <h4 className="card-title"><br/>T-Bone Steak &amp; Eggs<br/><br/></h4>
                                        <p className="card-text"><a
                                            href="https://demo.opeqe.com/location/beverly-hills/search/menu/pizza">Pizza</a><a
                                            href="https://demo.opeqe.com/location/beverly-hills/search/cuisine/italian">Italian</a><a
                                            href="https://demo.opeqe.com/location/beverly-hills/search/course/main-course">Main
                                            Course</a><br/></p>
                                        <div
                                            className="icons"><a href="#"><i className="icon-social-facebook"></i></a><a
                                            href="#"><i className="icon-social-instagram"></i></a><a href="#"><i
                                            className="icon-social-twitter"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-6 col-lg-4">
                                <div className="card clean-card text-center"><img className="card-img-top w-100 d-block"
                                                                                  src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo-2.png"/>
                                    <div className="card-body info">
                                        <h4 className="card-title">Robert Downturn</h4>
                                        <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing
                                            elit.</p>
                                        <div className="icons"><a href="#"><i
                                            className="icon-social-facebook"></i></a><a href="#"><i
                                            className="icon-social-instagram"></i></a><a href="#"><i
                                            className="icon-social-twitter"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-6 col-lg-4">
                                <div className="card clean-card text-center"><img className="card-img-top w-100 d-block"
                                                                                  src="../../assets/img/Screenshot_2020-06-02%20Opeqe%20Web%20App%20Demo-3.png"/>
                                    <div className="card-body info">
                                        <h4 className="card-title">Ally Sanders</h4>
                                        <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing
                                            elit.</p>
                                        <div className="icons"><a href="#"><i
                                            className="icon-social-facebook"></i></a><a href="#"><i
                                            className="icon-social-instagram"></i></a><a href="#"><i
                                            className="icon-social-twitter"></i></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
            </>
        );
    }

}

